# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qt/modulos/ui_control1.ui'
#
# Created: Sat May 18 03:51:10 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Masul(object):
    def setupUi(self, Masul):
        Masul.setObjectName(_fromUtf8("Masul"))
        Masul.resize(698, 480)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/actions/images/actions/color_18/window.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Masul.setWindowIcon(icon)
        self.verticalLayout_3 = QtGui.QVBoxLayout(Masul)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setMargin(0)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.topModule = QtGui.QWidget(Masul)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.topModule.sizePolicy().hasHeightForWidth())
        self.topModule.setSizePolicy(sizePolicy)
        self.topModule.setStyleSheet(_fromUtf8("#topModule{padding:10px}"))
        self.topModule.setObjectName(_fromUtf8("topModule"))
        self.verticalLayout_9 = QtGui.QVBoxLayout(self.topModule)
        self.verticalLayout_9.setMargin(0)
        self.verticalLayout_9.setObjectName(_fromUtf8("verticalLayout_9"))
        self._2 = QtGui.QHBoxLayout()
        self._2.setContentsMargins(9, -1, -1, -1)
        self._2.setObjectName(_fromUtf8("_2"))
        self.tbHome = QtGui.QToolButton(self.topModule)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tbHome.sizePolicy().hasHeightForWidth())
        self.tbHome.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.tbHome.setFont(font)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/actions/images/actions/color_18/home.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.tbHome.setIcon(icon1)
        self.tbHome.setIconSize(QtCore.QSize(18, 18))
        self.tbHome.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.tbHome.setObjectName(_fromUtf8("tbHome"))
        self._2.addWidget(self.tbHome)
        self.leTitulo = QtGui.QLabel(self.topModule)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.leTitulo.setFont(font)
        self.leTitulo.setText(_fromUtf8(""))
        self.leTitulo.setObjectName(_fromUtf8("leTitulo"))
        self._2.addWidget(self.leTitulo)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self._2.addItem(spacerItem)
        self.verticalLayout_9.addLayout(self._2)
        self.horizontalLayout_52 = QtGui.QHBoxLayout()
        self.horizontalLayout_52.setSpacing(3)
        self.horizontalLayout_52.setContentsMargins(10, -1, -1, -1)
        self.horizontalLayout_52.setObjectName(_fromUtf8("horizontalLayout_52"))
        self.tbLimpiar = QtGui.QToolButton(self.topModule)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tbLimpiar.sizePolicy().hasHeightForWidth())
        self.tbLimpiar.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.tbLimpiar.setFont(font)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/actions/images/actions/black_18/add.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.tbLimpiar.setIcon(icon2)
        self.tbLimpiar.setIconSize(QtCore.QSize(18, 18))
        self.tbLimpiar.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.tbLimpiar.setAutoRaise(True)
        self.tbLimpiar.setObjectName(_fromUtf8("tbLimpiar"))
        self.horizontalLayout_52.addWidget(self.tbLimpiar)
        self.tbModif = QtGui.QToolButton(self.topModule)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tbModif.sizePolicy().hasHeightForWidth())
        self.tbModif.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.tbModif.setFont(font)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8(":/actions/images/actions/black_18/diskette.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.tbModif.setIcon(icon3)
        self.tbModif.setIconSize(QtCore.QSize(18, 18))
        self.tbModif.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.tbModif.setAutoRaise(True)
        self.tbModif.setObjectName(_fromUtf8("tbModif"))
        self.horizontalLayout_52.addWidget(self.tbModif)
        spacerItem1 = QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_52.addItem(spacerItem1)
        self.verticalLayout_9.addLayout(self.horizontalLayout_52)
        self.verticalLayout_3.addWidget(self.topModule)
        self.splitter_2 = QtGui.QSplitter(Masul)
        self.splitter_2.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_2.setObjectName(_fromUtf8("splitter_2"))
        self.scrollArea = QtGui.QScrollArea(self.splitter_2)
        self.scrollArea.setMinimumSize(QtCore.QSize(300, 0))
        self.scrollArea.setFrameShape(QtGui.QFrame.NoFrame)
        self.scrollArea.setFrameShadow(QtGui.QFrame.Plain)
        self.scrollArea.setLineWidth(0)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName(_fromUtf8("scrollArea"))
        self.scrollAreaWidgetContents = QtGui.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 300, 400))
        self.scrollAreaWidgetContents.setObjectName(_fromUtf8("scrollAreaWidgetContents"))
        self.verticalLayout = QtGui.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.splitter = QtGui.QSplitter(self.scrollAreaWidgetContents)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.fDatos = QtGui.QFrame(self.splitter)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fDatos.sizePolicy().hasHeightForWidth())
        self.fDatos.setSizePolicy(sizePolicy)
        self.fDatos.setObjectName(_fromUtf8("fDatos"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.fDatos)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.lblDatos = QtGui.QLabel(self.fDatos)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lblDatos.sizePolicy().hasHeightForWidth())
        self.lblDatos.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.lblDatos.setFont(font)
        self.lblDatos.setAlignment(QtCore.Qt.AlignCenter)
        self.lblDatos.setWordWrap(True)
        self.lblDatos.setMargin(4)
        self.lblDatos.setObjectName(_fromUtf8("lblDatos"))
        self.verticalLayout_2.addWidget(self.lblDatos)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.lblInfo = QtGui.QLabel(self.fDatos)
        self.lblInfo.setWordWrap(True)
        self.lblInfo.setMargin(3)
        self.lblInfo.setObjectName(_fromUtf8("lblInfo"))
        self.horizontalLayout_3.addWidget(self.lblInfo)
        self.lblLogo = QtGui.QLabel(self.fDatos)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lblLogo.sizePolicy().hasHeightForWidth())
        self.lblLogo.setSizePolicy(sizePolicy)
        self.lblLogo.setMaximumSize(QtCore.QSize(16777215, 64))
        self.lblLogo.setText(_fromUtf8(""))
        self.lblLogo.setPixmap(QtGui.QPixmap(_fromUtf8("../../../../../../../Users/usr/share/pyventa/images/png/64-box_open.png")))
        self.lblLogo.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblLogo.setObjectName(_fromUtf8("lblLogo"))
        self.horizontalLayout_3.addWidget(self.lblLogo)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        self.frame = QtGui.QFrame(self.fDatos)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.frame)
        self.verticalLayout_4.setMargin(0)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.inDatos = QtGui.QFrame(self.frame)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.inDatos.sizePolicy().hasHeightForWidth())
        self.inDatos.setSizePolicy(sizePolicy)
        self.inDatos.setObjectName(_fromUtf8("inDatos"))
        self.formLay = QtGui.QFormLayout(self.inDatos)
        self.formLay.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLay.setObjectName(_fromUtf8("formLay"))
        self.verticalLayout_4.addWidget(self.inDatos)
        self.verticalLayout_2.addWidget(self.frame)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem2)
        self.verticalLayout.addWidget(self.splitter)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.tvTabla = QtGui.QTableView(self.splitter_2)
        self.tvTabla.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.tvTabla.setAlternatingRowColors(True)
        self.tvTabla.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.tvTabla.setGridStyle(QtCore.Qt.DashLine)
        self.tvTabla.setSortingEnabled(True)
        self.tvTabla.setObjectName(_fromUtf8("tvTabla"))
        self.tvTabla.verticalHeader().setVisible(False)
        self.verticalLayout_3.addWidget(self.splitter_2)

        self.retranslateUi(Masul)
        QtCore.QMetaObject.connectSlotsByName(Masul)

    def retranslateUi(self, Masul):
        Masul.setWindowTitle(_translate("Masul", "Control administrativo", None))
        self.tbHome.setText(_translate("Masul", "Tienda", None))
        self.tbHome.setShortcut(_translate("Masul", "Home", None))
        self.tbLimpiar.setToolTip(_translate("Masul", "Nuevo producto", None))
        self.tbLimpiar.setText(_translate("Masul", "&Nuevo", None))
        self.tbLimpiar.setShortcut(_translate("Masul", "Ctrl+N", None))
        self.tbModif.setToolTip(_translate("Masul", "Guardar", None))
        self.tbModif.setText(_translate("Masul", "Guardar", None))
        self.tbModif.setShortcut(_translate("Masul", "Ctrl+G", None))
        self.lblDatos.setText(_translate("Masul", "Detalle de elemento", None))
        self.lblInfo.setText(_translate("Masul", "Titulo del objeto", None))

import icons_rc
